/*
 Description:
 Implement a function that will get a pointer to a one-way-linked list head and return an array of two linked lists.
 First, of them, they should contain only odd elements from the input, while second only even elements. Both returned lists should be in a reversed order
 *IOS should use Swift
 */

import Foundation

class ListNode {
    var value: Int
    var next: ListNode?
    
    init(value: Int, next: ListNode?) {
        self.value = value
        self.next = next
    }
}

func printList(head: ListNode?) {
    var currentNode = head
    while currentNode != nil {
        print(currentNode?.value ?? -1)
        currentNode = currentNode?.next
    }
}

func reverseList(head: ListNode?) -> ListNode? {
    var currentNode = head
    var prev: ListNode?
    var next: ListNode?
    
    while currentNode != nil {
        next = currentNode?.next
        currentNode?.next = prev
        prev = currentNode
        currentNode = next
    }
    
    return prev
}

func segregateEvenOdd(head: ListNode?) -> [ListNode]? {
    var currentNode = head
    var evenStart: ListNode?
    var evenEnd: ListNode?
    var oddStart: ListNode?
    var oddEnd: ListNode?
    var result = [ListNode]()
    
    while let node = currentNode {
        let value = node.value
        if (value % 2 == 0) {
            if (evenStart == nil) {
                evenStart = node
                evenEnd = evenStart
            } else {
                evenEnd?.next = node
                evenEnd = evenEnd?.next
            }
        } else {
            if oddStart == nil {
                oddStart = node
                oddEnd = oddStart
            } else {
                oddEnd?.next = node
                oddEnd = oddEnd?.next
            }
        }
        
        currentNode = node.next
    }
    
    evenEnd?.next = nil
    oddEnd?.next = nil
    
    if let evenReversed = reverseList(head: evenStart) {
        result.append(evenReversed)
    }
    if let oddReversed = reverseList(head: oddStart) {
        result.append(oddReversed)
    }
    
    return result
}

let node6 = ListNode(value: 3, next: nil)
let node5 = ListNode(value: 20, next: node6)
let node4 = ListNode(value: 11, next: node5)
let node3 = ListNode(value: 9, next: node4)
let node2 = ListNode(value: 4, next: node3)
let node1 = ListNode(value: 0, next: node2)
let head = ListNode(value: 1, next: node1)

let lists = segregateEvenOdd(head: head)
lists?.forEach { printList(head: $0) }
